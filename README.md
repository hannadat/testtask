# testtask
A test project with 2 sample UI tests.

Tools used: Selenium Webdriver, NUnit, Fluent Assertions, Specflow.

All configurable parameters are in app.config file.

Nuget dependencies are in packages.config.

Project contains nunit3 console runner. To run tests from cdm use following command from '..\bin\Debug' folder: "nunit3-console TestTask.dll --result Result.xml".
To provide additional settings from command line, use following: "nunit3-console TestTask.dll --testparam browser=firefox --testparam width=800 --testparam height=600 --testparam implicttimeout=2".

To run tests from Visual Studio, install "NUnit3 Test Adapter" Visual Studio extension: https://marketplace.visualstudio.com/items?itemName=NUnitDevelopers.NUnit3TestAdapter.

To be able to use Specflow features in Visual Studio, install Specflow extension: https://marketplace.visualstudio.com/items?itemName=TechTalkSpecFlowTeam.SpecFlowforVisualStudio2017.