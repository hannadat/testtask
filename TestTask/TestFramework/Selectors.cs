﻿using OpenQA.Selenium;

namespace TestTask.TestFramework
{
    public static class Selectors
    {
        public static class GooglePage
        {
            public static readonly By Searchbox = By.CssSelector("input[name='q']");
        }

        public static class SearchResultsPage
        {
            public static By NextPageButton = By.CssSelector("a#pnnext");
            public static By SearchResult = By.CssSelector("div.srg div.g");
        }

        public static class SearchResult
        {
            public static By LinkElement = By.CssSelector("div.r>a");
        }
    }
}
