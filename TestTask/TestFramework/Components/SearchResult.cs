﻿using System;
using OpenQA.Selenium;

namespace TestTask.TestFramework
{
    public class SearchResult
    {
        private IWebElement _container;
        private IWebDriver Driver => DriverManager.Instance.GetDriver();
        private IWebElement LinkElement => _container.FindElement(Selectors.SearchResult.LinkElement);

        public SearchResult(IWebElement webElement)
        {
            _container = webElement;
        }

        public Uri Link => new Uri(LinkElement.GetAttribute("href"));

        public TargetPage Open()
        {
            Logger.WriteLine("Opening search result");
            LinkElement.Click();
            return new TargetPage();
        }
    }
}