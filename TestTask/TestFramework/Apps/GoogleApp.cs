﻿using OpenQA.Selenium;

namespace TestTask.TestFramework
{
    public class GoogleApp: PageBase
    {
        private static GoogleApp _googleApp = null;

        private SearchResultsPage _searchResults;

        private GoogleApp() { }

        private IWebElement SearchBox => Driver.FindElement(Selectors.GooglePage.Searchbox);

        public GoogleApp Open()
        {
            Driver.Navigate().GoToUrl(Settings.GoogleHost);
            Logger.WriteLine("Opened Google page.");
            return Instance;
        }

        public SearchResultsPage Search(string keyWord)
        {
            Logger.WriteLine($"Search for keyword {keyWord}");
            SearchBox.SendKeys(keyWord);
            SearchBox.Submit();
            return SearchResults;
        }

        public SearchResultsPage SearchResults => _searchResults ?? (_searchResults = new SearchResultsPage());

        public static GoogleApp Instance => _googleApp ?? (_googleApp = new GoogleApp());
    }
}
