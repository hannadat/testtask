﻿using System;

namespace TestTask.TestFramework
{
    public static class Logger
    {
        public static void WriteLine(string message, bool withTimeStampt = true)
        {
            Console.WriteLine(withTimeStampt ? $"{DateTime.Now.ToShortTimeString()}: {message}": message);
        }
    }
}
