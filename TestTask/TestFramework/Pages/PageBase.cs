﻿using OpenQA.Selenium;

namespace TestTask.TestFramework
{
    public abstract class PageBase
    {
        protected IWebDriver Driver => DriverManager.Instance.GetDriver();
    }
}
