﻿namespace TestTask.TestFramework
{
    public class TargetPage:PageBase
    {
        public string Title => Driver.Title;
    }
}