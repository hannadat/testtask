﻿using System.Collections.Generic;
using OpenQA.Selenium;
using System.Linq;

namespace TestTask.TestFramework
{
    public class SearchResultsPage :PageBase
    {
        private IWebElement NextPageButton => Driver.FindElement(Selectors.SearchResultsPage.NextPageButton);

        public List<SearchResult> Results
        {
            get
            {
                var elements = Driver.FindElements(Selectors.SearchResultsPage.SearchResult);
                return elements.Select(el => new SearchResult(el)).ToList();
            }
        }

        public SearchResultsPage NextPage()
        {
            Logger.WriteLine("Switching to next page with search results");
            NextPageButton.Click();
            return this;
        }
    }
}