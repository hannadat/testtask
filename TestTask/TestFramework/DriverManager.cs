﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;

namespace TestTask.TestFramework
{
    public class DriverManager
    {
        private IWebDriver _driver;
        private static DriverManager _manager = null;

        private DriverManager()
        {
        }

        private IWebDriver InitDriver()
        {
            IWebDriver driver;
            switch (Settings.Browser)
            {
                case Browser.Firefox:
                    driver = new FirefoxDriver();
                    break;
                case Browser.Chrome:
                default:
                    driver = new ChromeDriver();
                    break;
            }
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMilliseconds(Settings.DriverImplicitTimeout);
            driver.Manage().Window.Size = new System.Drawing.Size(Settings.BrowserWidth, Settings.BrowserHeight);
            Logger.WriteLine("Initialized an instance of WebDriver");
            return driver;
        }

        public IWebDriver GetDriver()
        {
            return _driver ?? (_driver = InitDriver());
        }

        public void QuitDriver()
        {
            Logger.WriteLine("Quitting WebDriver");
            _driver.Quit();
            _driver = null;
        }

        public static DriverManager Instance =>  _manager ?? (_manager = new DriverManager());
    }

    public enum Browser
    {
        Chrome,
        Firefox
    }
}
