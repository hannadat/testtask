﻿using TechTalk.SpecFlow;
using TestTask.TestFramework;

namespace TestTask
{
    [Binding]
    public sealed class TestsSetup
    {
        [AfterTestRun]
        public static void AfterTestRun()
        {
            DriverManager.Instance.QuitDriver();
        }
    }
}
