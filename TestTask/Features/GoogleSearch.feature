﻿Feature: GoogleSearch
	In order to find relative information in the Internet
	As user of Google
	I want to be able to search by keyword

Background: 
	Given I have opened Google

Scenario: Check title of search result page
	When I search for "automation" keyword
	And I open first search result
	Then I see that page title contains "automation" keyword

Scenario: Check for domain in search results
	When I search for "automation" keyword
	And I look through domains of first 5 pages of search results
	Then I see that domains contain "testautomationday.com" domain