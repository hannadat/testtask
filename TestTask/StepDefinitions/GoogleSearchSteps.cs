﻿using FluentAssertions;
using System.Collections.Generic;
using System.Linq;
using TechTalk.SpecFlow;
using TestTask.TestFramework;

namespace TestTask.StepDefinitions
{
    [Binding]
    public sealed class GoogleSearchSteps
    {
        private GoogleApp Google => GoogleApp.Instance;
        private TargetPage _targetPage = null;
        private List<string> hosts = new List<string>();

        [Given(@"I have opened Google")]
        public void GivenIHaveOpenedGoogle()
        {
            Google.Open();
        }

        [When(@"I search for ""(.*)"" keyword")]
        public void WhenISearchForKeyword(string keyWord)
        {
            Google.Search(keyWord);
        }

        [When(@"I open first search result")]
        public void WhenIOpenFirstSearchResult()
        {
            _targetPage = Google.SearchResults.Results.First().Open();
        }

        [Then(@"I see that page title contains ""(.*)"" keyword")]
        public void ThenISeeThatPageHasTitle(string keyWord)
        {
            _targetPage.Title.ToLower().Should().Contain(keyWord, $"Page title should contain {keyWord}");
        }

        [When(@"I look through domains of first (.*) pages of search results")]
        public void WhenILookThroughDomainsOfFirstPagesOfSearchResults(int numberOfPages)
        {
            hosts.AddRange(Google.SearchResults.Results.Select(result => result.Link.Host));
            for (int i = 0; i < numberOfPages-1; i++)
            {
                var results = Google.SearchResults.NextPage().Results;
                hosts.AddRange(results.Select(result => result.Link.Host));
            }
        }

        [Then(@"I see that domains contain ""(.*)"" domain")]
        public void ThenISeeThatDomainsContainDomain(string domain)
        {
            hosts.Exists(host => host.Contains(domain)).Should().BeTrue($"Search result items should contain '{domain}' domain.");
        }

    }
}
