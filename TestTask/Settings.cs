﻿using System;
using System.Configuration;
using TestTask.TestFramework;
using NUnit.Framework;

namespace TestTask
{
    public static class Settings
    {
        private static Browser GetBrowser()
        {
            switch (TestContext.Parameters.Get("browser") ?? ConfigurationManager.AppSettings["Browser"].ToLower())
            {
                case "chrome": return Browser.Chrome;
                case "firefox": return Browser.Firefox;
                default:
                    throw new ArgumentException("Unknown browser type");

            }
        }

        public static string GoogleHost => TestContext.Parameters.Get("googlehost") ?? ConfigurationManager.AppSettings["GoogleHost"];
        public static int BrowserWidth => int.Parse(TestContext.Parameters.Get("width") ?? ConfigurationManager.AppSettings["BrowserWidth"]);
        public static int BrowserHeight => int.Parse(TestContext.Parameters.Get("height") ?? ConfigurationManager.AppSettings["BrowserHeight"]);
        public static Browser Browser => GetBrowser();
        public static double DriverImplicitTimeout => double.Parse(TestContext.Parameters.Get("implicittimeout") ?? ConfigurationManager.AppSettings["ImplicitTimeout"]);
    }
}
